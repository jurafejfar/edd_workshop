--
-- Name: addresses_raw; Type: TABLE; Schema: @extschema@; Owner: -
--

CREATE TABLE @extschema@.addresses_raw (
    gid bigint NOT NULL,
    country text,
    city text,
    place text,
    street text,
    postcode text,
    housenumber text,
    conscriptionnumber text,
    geom public.geometry(Point,5514)
);


--
-- Name: addresses; Type: VIEW; Schema: @extschema@; Owner: -
--

CREATE VIEW @extschema@.addresses AS
 SELECT min(addresses_raw.gid) AS gid,
    count(*) AS count,
    (array_remove(array_agg(DISTINCT addresses_raw.country), NULL::text))[1] AS country,
    (array_remove(array_agg(DISTINCT addresses_raw.city), NULL::text))[1] AS city,
    (array_remove(array_agg(DISTINCT addresses_raw.place), NULL::text))[1] AS place,
    addresses_raw.street,
    (array_remove(array_agg(DISTINCT addresses_raw.postcode), NULL::text))[1] AS postcode,
    addresses_raw.housenumber,
    (array_remove(array_agg(DISTINCT addresses_raw.conscriptionnumber), NULL::text))[1] AS conscriptionnumber,
    (public.st_collect(addresses_raw.geom))::public.geometry(MultiPoint,5514) AS geom
   FROM @extschema@.addresses_raw
  GROUP BY addresses_raw.street, addresses_raw.housenumber;


--
-- Name: buildings; Type: TABLE; Schema: @extschema@; Owner: -
--

CREATE TABLE @extschema@.buildings (
    gid bigint NOT NULL,
    country text,
    city text,
    place text,
    street text,
    postcode text,
    housenumber text,
    conscriptionnumber text,
    building text,
    geom public.geometry(Geometry,5514)
);


--
-- Name: clenove; Type: TABLE; Schema: @extschema@; Owner: -
--

CREATE TABLE @extschema@.clenove (
    osobni_cislo integer NOT NULL,
    c_domacnosti integer,
    prijmeni character varying(30),
    jmeno character varying(30),
    datum_narozeni date,
    trzby integer
);


--
-- Name: domacnosti; Type: TABLE; Schema: @extschema@; Owner: -
--

CREATE TABLE @extschema@.domacnosti (
    id integer,
    ulice_a_cp character varying(30),
    psc text,
    mesto character varying(30),
    obvod integer NOT NULL
);


--
-- Name: jmenom; Type: TABLE; Schema: @extschema@; Owner: -
--

--
-- Name: kategorie; Type: TABLE; Schema: @extschema@; Owner: -
--

CREATE TABLE @extschema@.kategorie (
    id integer NOT NULL,
    vek_od integer,
    vek_do integer,
    nazev character varying(20)
);

insert into @extschema@.kategorie VALUES (1, 0, 6, 'dítě');
insert into @extschema@.kategorie VALUES (2, 6, 18, 'školák');
insert into @extschema@.kategorie VALUES (3, 18, 60, 'dospělý');
insert into @extschema@.kategorie VALUES (4, 60, 200, 'senior');

SELECT pg_catalog.pg_extension_config_dump('@extschema@.kategorie', '');
--SELECT pg_catalog.pg_extension_config_dump('@extschema@.kategorie', '');

--
-- Name: roads; Type: TABLE; Schema: @extschema@; Owner: -
--

CREATE TABLE @extschema@.roads (
    gid bigint NOT NULL,
    route text,
    highway text,
    geom public.geometry(LineString,5514)
);


--
-- Name: v_clenove_adresy; Type: VIEW; Schema: @extschema@; Owner: -
--

CREATE VIEW @extschema@.v_clenove_adresy AS
 SELECT clenove.osobni_cislo,
    clenove.c_domacnosti,
    clenove.prijmeni,
    clenove.jmeno,
    clenove.datum_narozeni,
    clenove.trzby,
    domacnosti.id,
    domacnosti.ulice_a_cp,
    domacnosti.psc,
    domacnosti.mesto,
    domacnosti.obvod,
    kategorie.nazev,
    addresses.gid,
    addresses.count,
    addresses.country,
    addresses.city,
    addresses.place,
    addresses.street,
    addresses.postcode,
    addresses.housenumber,
    addresses.conscriptionnumber,
    addresses.geom
   FROM (((@extschema@.clenove
     JOIN @extschema@.domacnosti ON ((clenove.c_domacnosti = domacnosti.id)))
     LEFT JOIN @extschema@.addresses ON (((COALESCE(domacnosti.psc, 'bez_psc'::text) = COALESCE(addresses.postcode, 'bez_psc'::text)) AND ((domacnosti.ulice_a_cp)::text = concat(addresses.street, ' ', addresses.housenumber)))))
     LEFT JOIN @extschema@.kategorie ON (((date_part('year'::text, age((clenove.datum_narozeni)::timestamp with time zone)) >= (kategorie.vek_od)::double precision) AND (date_part('year'::text, age((clenove.datum_narozeni)::timestamp with time zone)) < (kategorie.vek_do)::double precision))));

--
-- Name: addresses_raw addresses_pkey; Type: CONSTRAINT; Schema: @extschema@; Owner: -
--

ALTER TABLE ONLY @extschema@.addresses_raw
    ADD CONSTRAINT addresses_pkey PRIMARY KEY (gid);


--
-- Name: buildings buildings_pkey; Type: CONSTRAINT; Schema: @extschema@; Owner: -
--

ALTER TABLE ONLY @extschema@.buildings
    ADD CONSTRAINT buildings_pkey PRIMARY KEY (gid);


--
-- Name: clenove clenove_pkey; Type: CONSTRAINT; Schema: @extschema@; Owner: -
--

ALTER TABLE ONLY @extschema@.clenove
    ADD CONSTRAINT clenove_pkey PRIMARY KEY (osobni_cislo);


--
-- Name: kategorie kategorie_pkey; Type: CONSTRAINT; Schema: @extschema@; Owner: -
--

ALTER TABLE ONLY @extschema@.kategorie
    ADD CONSTRAINT kategorie_pkey PRIMARY KEY (id);


--
-- Name: roads roads_pkey; Type: CONSTRAINT; Schema: @extschema@; Owner: -
--

ALTER TABLE ONLY @extschema@.roads
    ADD CONSTRAINT roads_pkey PRIMARY KEY (gid);
